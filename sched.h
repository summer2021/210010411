
#include <uapi/linux/time.h>

struct time_statistics{
	struct timeval *in_cpu;		    	//pick_next_task_fair
	struct timeval *out_cpu;
	struct timeval *in_context_switch;	//context
	struct timeval *out_context_switch;	//_schedule
	struct timeval *in_queue_start;		//equeue_task_fair
	struct timeval *in_queue_end;
	struct timeval *out_queue_start;	//dequeue_task_fair
	struct timeval *out_queue_end;
	struct timeval *p_n_t_start;		//pick_next_task
	struct timeval *p_n_t_end;
	struct timeval *next_p_n_t_start;
};

struct task_struct {
	
	struct time_statistics *time_s;
	
	/*
	 *
	 */
}
